package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {

	private static final int MAX_ROLLS = 20;
	private int[] rolls = new int[20];
	private int currentRoll = 0;
	private int strikeCont = 0;
	private boolean strike = false;

	@Override
	public void roll(int pins) {
		rolls[currentRoll++] = pins;
	}

	@Override
	public int score() {
		int score = 0;
		for(int currentRoll=0; currentRoll<MAX_ROLLS; currentRoll++){
			if(isStrike(currentRoll))
				score += rolls[currentRoll];
			if (isSpare(currentRoll))
				score += rolls[currentRoll+2];
			score += rolls[currentRoll];
		}
		return score;
	}
	
	private boolean isSpare(int currentRoll){
		return currentRoll < MAX_ROLLS-1 && rolls[currentRoll] + rolls[currentRoll+1] == 10  && currentRoll%2==0;
	}
	
	private boolean isStrike(int currentRoll){
		if(strikeCont==1){
			strikeCont+=1;
			strike=true;
		}
		if(strikeCont==2){
			strikeCont=0;
			strike=true;
		}
		if (rolls[currentRoll]==10 && strikeCont == 0 && currentRoll%2==0){
			strikeCont+=1;
			strike=false;
		}
		return strike;
	}

}
